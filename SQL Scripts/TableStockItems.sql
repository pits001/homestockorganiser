USE [HomeOrganiser]
GO

ALTER TABLE [dbo].[StockItems] DROP CONSTRAINT [DF_StockItems_AddToShoppingList]
GO

ALTER TABLE [dbo].[StockItems] DROP CONSTRAINT [DF_StockItems_ItemIncrement]
GO

ALTER TABLE [dbo].[StockItems] DROP CONSTRAINT [DF_StockItems_MinimumStockLevel]
GO

ALTER TABLE [dbo].[StockItems] DROP CONSTRAINT [DF_StockItems_ItemsInStock]
GO

/****** Object:  Table [dbo].[StockItems]    Script Date: 31/03/2017 1:13:24 PM ******/
DROP TABLE [dbo].[StockItems]
GO

/****** Object:  Table [dbo].[StockItems]    Script Date: 31/03/2017 1:13:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StockItems](
	[SI_id] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [nvarchar](150) NOT NULL,
	[ItemDescription] [nvarchar](512) NULL,
	[ItemBarcode] [nvarchar](150) NULL,
	[ItemsInStock] [decimal](18, 0) NOT NULL,
	[MinimumStockLevel] [decimal](18, 0) NOT NULL,
	[ItemIncrement] [decimal](18, 0) NOT NULL,
	[AddToShoppingList] [bit] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StockItems] ADD  CONSTRAINT [DF_StockItems_ItemsInStock]  DEFAULT ((0)) FOR [ItemsInStock]
GO

ALTER TABLE [dbo].[StockItems] ADD  CONSTRAINT [DF_StockItems_MinimumStockLevel]  DEFAULT ((0)) FOR [MinimumStockLevel]
GO

ALTER TABLE [dbo].[StockItems] ADD  CONSTRAINT [DF_StockItems_ItemIncrement]  DEFAULT ((0)) FOR [ItemIncrement]
GO

ALTER TABLE [dbo].[StockItems] ADD  CONSTRAINT [DF_StockItems_AddToShoppingList]  DEFAULT ((1)) FOR [AddToShoppingList]
GO


